﻿$clientID = "decf3d88-faa9-452b-b8ff-be189ae749c9"
$key = "[tbyKJuDw***qdXxSftiijdkkQUK0906"
$SecurePassword = $key | ConvertTo-SecureString -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential `
-argumentlist $clientID, $SecurePassword
$tenantID = "0ab4cbbf-4bc7-4826-b52c-a14fed5286b9"

Connect-AzureRmAccount -Credential $cred -TenantId $tenantID -ServicePrincipal

$resourceGroupName = "darwinpoc";
$dataFactoryName = "darwinpoc-adf-SIT";
  
New-AzureRmResourceGroupDeployment  -ResourceGroupName darwinpoc -Name ADF-Deploy-DarwinPOC -TemplateFile ".\ARMTemplateForFactory.json" -TemplateParameterFile ".\ARMTemplateParametersForFactory.json"     –force
  




